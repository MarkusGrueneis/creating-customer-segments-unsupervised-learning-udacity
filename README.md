# Creating Customer Segments - Unsupervised Learning - UDACITY

In this project, two datasets with demographic information about the people of Germany, and with that same information for customers of a mail-order sales company have been provided by Udacity Bertelsmann partners AZ Direct and Arvato Financial Solutions.
In the analysis, I look at relationships between demographics features in the dataset.
Further, the population can be classified into clusters, and we can see how the customers are distributed in each of the identified segments.

## Project Motivation

This is the third project of Udacity Data Scientist Nanodegree. In this project, I applied unsupervised learning techniques to identify segments of the population that form the core customer base for a mail-order sales company in Germany. These segments can then be used to direct marketing campaigns towards audiences that will have the highest expected rate of returns.

## File Descriptions

There is one iPython notebook to showcase work related to this project.

The html file was generated from iPython notebook to submit this project to udacity.


## Acknowledgements

Credits must be given to Udacity for providing starting code for this project. The data was provided by Udacity partners at Bertelsmann Arvato Analytics.
